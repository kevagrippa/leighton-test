export class Product {       
    public productid: string;
    public name: string;
    public brand: string;
    public colour: string;
    public description: string;
    public price: number;
    public size: {
        small: number,
        medium: number,
        large: number
    }
 
}
export class ProductResponse {
    Count: number;
    Items: Product[]
}