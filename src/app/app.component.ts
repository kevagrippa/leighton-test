import { Component} from '@angular/core';
import { Product } from './Product/product.model';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  viewBasket: boolean = false;
  basketList: Product [] = [];
  total: number = 0;
  count: number = 0;

  toggleBasket(){
    this.viewBasket = !this.viewBasket;
  }

  addToBasket(product: Product){
    this.count = 0;
    this.total += product.price;
    this.basketList = [...this.basketList, product];
    this.count += this.basketList.length;
  }

  emptyBasket(){
    this.basketList = [];
    this.total = 0;
    this.count = 0;
  }

  constructor (){}
}

