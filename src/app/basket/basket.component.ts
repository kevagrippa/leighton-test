import { Component, Input, Output, EventEmitter} from '@angular/core';
import { Product } from '../Product/product.model';


@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.scss']
})
export class BasketComponent {
  @Input() viewBasket: boolean;
  @Input() basketList: Product [] = [];
  @Output() clearBasket:EventEmitter<null> = new EventEmitter<null>();
  @Input() total: number = 0;
  
  public emptyBasket(){
    this.clearBasket.emit();
  }

  constructor() {}
}
