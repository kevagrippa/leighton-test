import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  getProducts (): Observable<Object> {
    const headers: HttpHeaders = new HttpHeaders().set('x-api-key', 'zQo4PPqD862IwDIQRZub8gX4dqjA3aW2DDhI6UF4');
    return this.http.get('https://27gmrimn45.execute-api.eu-west-2.amazonaws.com/demos/leighton-demo-api?TableName=products', {headers: headers});
  }
}
