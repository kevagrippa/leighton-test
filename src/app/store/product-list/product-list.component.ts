import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Product } from 'src/app/Product/product.model';


@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent {
  @Input() product: Product;
  @Input() featured: boolean;
  @Output() productSelected:EventEmitter<Product> = new EventEmitter<Product>();
  @Input() selectedSize: string = null;

  addToBasket(){
    let newProduct = {...this.product,chosenSize:this.selectedSize}
    this.productSelected.emit(newProduct);
  }

  constructor(){}
}
