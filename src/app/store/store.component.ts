import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ProductService } from '../product.service';
import { Product, ProductResponse } from '../Product/product.model';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss']
})
export class StoreComponent implements OnInit {
  featured: Product[] = []
  productList: Product[] = []
  @Output() newProductSelected = new EventEmitter<Product>();
  @Input() searchItem: string = "";


  onProductSelected(product: Product){
    this.newProductSelected.emit(product);
  }

  searchProducts(searchItem: string){
    if(this.searchItem !== ""){
      this.productService.getProducts()
      .subscribe((productResponse: ProductResponse) => {
        productResponse.Items.map((product: Product) => product.price = Math.floor(Math.random()*100));
        this.productList = productResponse.Items.filter((product: Product) => product.name.includes(this.searchItem));
        this.featured = null;
      });
    } else {
      this.getProduct();
    }
  }

  getProduct(){
    this.productService.getProducts()
    .subscribe((productResponse: ProductResponse) => {
      productResponse.Items.map((product: Product) => product.price = Math.floor(Math.random()*100));
      this.featured = productResponse.Items.filter((product: Product) => product.productid === '0m8hjmd721');
      this.productList = productResponse.Items.filter((product: Product) => product.productid !== '0m8hjmd721');
    });
  }

  constructor (
    private productService: ProductService
  ){}
  
  ngOnInit(){
    this.getProduct();
  }
}
  


